
#include <stdlib.h>
#include <string.h>
#include "sc_types.h"
#include "Blinky.h"
#include "BlinkyRequired.h"
/*! \file Implementation of the state machine 'blinky'
*/

/* prototypes of all internal functions */
static sc_boolean blinky_check_main_region_ON_tr0_tr0(const Blinky* handle);
static sc_boolean blinky_check_main_region_ON_tr1_tr1(const Blinky* handle);
static sc_boolean blinky_check_main_region_OFF_tr0_tr0(const Blinky* handle);
static sc_boolean blinky_check_main_region_OFF_tr1_tr1(const Blinky* handle);
static void blinky_effect_main_region_ON_tr0(Blinky* handle);
static void blinky_effect_main_region_ON_tr1(Blinky* handle);
static void blinky_effect_main_region_OFF_tr0(Blinky* handle);
static void blinky_effect_main_region_OFF_tr1(Blinky* handle);
static void blinky_enact_main_region_ON(Blinky* handle);
static void blinky_enact_main_region_OFF(Blinky* handle);
static void blinky_enseq_main_region_ON_default(Blinky* handle);
static void blinky_enseq_main_region_OFF_default(Blinky* handle);
static void blinky_enseq_main_region_default(Blinky* handle);
static void blinky_exseq_main_region_ON(Blinky* handle);
static void blinky_exseq_main_region_OFF(Blinky* handle);
static void blinky_exseq_main_region(Blinky* handle);
static void blinky_react_main_region_ON(Blinky* handle);
static void blinky_react_main_region_OFF(Blinky* handle);
static void blinky_react_main_region__entry_Default(Blinky* handle);
static void blinky_clearInEvents(Blinky* handle);
static void blinky_clearOutEvents(Blinky* handle);

const sc_integer BLINKY_BLINKYINTERNAL_MS_DELAY = 200;

void blinky_init(Blinky* handle)
{
	sc_integer i;

	for (i = 0; i < BLINKY_MAX_ORTHOGONAL_STATES; ++i)
	{
		handle->stateConfVector[i] = Blinky_last_state;
	}
	
	
	handle->stateConfVectorPosition = 0;

	blinky_clearInEvents(handle);
	blinky_clearOutEvents(handle);

	/* Default init sequence for statechart blinky */
	handle->internal.iTick = 0;

}

void blinky_enter(Blinky* handle)
{
	/* Default enter sequence for statechart blinky */
	blinky_enseq_main_region_default(handle);
}

void blinky_exit(Blinky* handle)
{
	/* Default exit sequence for statechart blinky */
	blinky_exseq_main_region(handle);
}

sc_boolean blinky_isActive(const Blinky* handle)
{
	sc_boolean result;
	if (handle->stateConfVector[0] != Blinky_last_state)
	{
		result =  bool_true;
	}
	else
	{
		result = bool_false;
	}
	return result;
}

/* 
 * Always returns 'false' since this state machine can never become final.
 */
sc_boolean blinky_isFinal(const Blinky* handle)
{
   return bool_false;
}

static void blinky_clearInEvents(Blinky* handle)
{
	handle->iface.evTick_raised = bool_false;
}

static void blinky_clearOutEvents(Blinky* handle)
{
}

void blinky_runCycle(Blinky* handle)
{
	
	blinky_clearOutEvents(handle);
	
	for (handle->stateConfVectorPosition = 0;
		handle->stateConfVectorPosition < BLINKY_MAX_ORTHOGONAL_STATES;
		handle->stateConfVectorPosition++)
		{
			
		switch (handle->stateConfVector[handle->stateConfVectorPosition])
		{
		case Blinky_main_region_ON :
		{
			blinky_react_main_region_ON(handle);
			break;
		}
		case Blinky_main_region_OFF :
		{
			blinky_react_main_region_OFF(handle);
			break;
		}
		default:
			break;
		}
	}
	
	blinky_clearInEvents(handle);
}


sc_boolean blinky_isStateActive(const Blinky* handle, BlinkyStates state)
{
	sc_boolean result = bool_false;
	switch (state)
	{
		case Blinky_main_region_ON :
			result = (sc_boolean) (handle->stateConfVector[0] == Blinky_main_region_ON
			);
			break;
		case Blinky_main_region_OFF :
			result = (sc_boolean) (handle->stateConfVector[0] == Blinky_main_region_OFF
			);
			break;
		default:
			result = bool_false;
			break;
	}
	return result;
}

void blinkyIface_raise_evTick(Blinky* handle)
{
	handle->iface.evTick_raised = bool_true;
}



/* implementations of all internal functions */

static sc_boolean blinky_check_main_region_ON_tr0_tr0(const Blinky* handle)
{
	return ((handle->iface.evTick_raised) && (handle->internal.iTick == BLINKY_BLINKYINTERNAL_MS_DELAY)) ? bool_true : bool_false;
}

static sc_boolean blinky_check_main_region_ON_tr1_tr1(const Blinky* handle)
{
	return ((handle->iface.evTick_raised) && (handle->internal.iTick < BLINKY_BLINKYINTERNAL_MS_DELAY)) ? bool_true : bool_false;
}

static sc_boolean blinky_check_main_region_OFF_tr0_tr0(const Blinky* handle)
{
	return ((handle->iface.evTick_raised) && (handle->internal.iTick == BLINKY_BLINKYINTERNAL_MS_DELAY)) ? bool_true : bool_false;
}

static sc_boolean blinky_check_main_region_OFF_tr1_tr1(const Blinky* handle)
{
	return ((handle->iface.evTick_raised) && (handle->internal.iTick < BLINKY_BLINKYINTERNAL_MS_DELAY)) ? bool_true : bool_false;
}

static void blinky_effect_main_region_ON_tr0(Blinky* handle)
{
	blinky_exseq_main_region_ON(handle);
	handle->internal.iTick = 0;
	blinky_enseq_main_region_OFF_default(handle);
}

static void blinky_effect_main_region_ON_tr1(Blinky* handle)
{
	blinky_exseq_main_region_ON(handle);
	handle->internal.iTick = handle->internal.iTick + 1;
	blinky_enseq_main_region_ON_default(handle);
}

static void blinky_effect_main_region_OFF_tr0(Blinky* handle)
{
	blinky_exseq_main_region_OFF(handle);
	handle->internal.iTick = 0;
	blinky_enseq_main_region_ON_default(handle);
}

static void blinky_effect_main_region_OFF_tr1(Blinky* handle)
{
	blinky_exseq_main_region_OFF(handle);
	handle->internal.iTick = handle->internal.iTick + 1;
	blinky_enseq_main_region_OFF_default(handle);
}

/* Entry action for state 'ON'. */
static void blinky_enact_main_region_ON(Blinky* handle)
{
	/* Entry action for state 'ON'. */
	blinkyIface_opLED(handle, bool_true);
}

/* Entry action for state 'OFF'. */
static void blinky_enact_main_region_OFF(Blinky* handle)
{
	/* Entry action for state 'OFF'. */
	blinkyIface_opLED(handle, bool_false);
}

/* 'default' enter sequence for state ON */
static void blinky_enseq_main_region_ON_default(Blinky* handle)
{
	/* 'default' enter sequence for state ON */
	blinky_enact_main_region_ON(handle);
	handle->stateConfVector[0] = Blinky_main_region_ON;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for state OFF */
static void blinky_enseq_main_region_OFF_default(Blinky* handle)
{
	/* 'default' enter sequence for state OFF */
	blinky_enact_main_region_OFF(handle);
	handle->stateConfVector[0] = Blinky_main_region_OFF;
	handle->stateConfVectorPosition = 0;
}

/* 'default' enter sequence for region main region */
static void blinky_enseq_main_region_default(Blinky* handle)
{
	/* 'default' enter sequence for region main region */
	blinky_react_main_region__entry_Default(handle);
}

/* Default exit sequence for state ON */
static void blinky_exseq_main_region_ON(Blinky* handle)
{
	/* Default exit sequence for state ON */
	handle->stateConfVector[0] = Blinky_last_state;
	handle->stateConfVectorPosition = 0;
}

/* Default exit sequence for state OFF */
static void blinky_exseq_main_region_OFF(Blinky* handle)
{
	/* Default exit sequence for state OFF */
	handle->stateConfVector[0] = Blinky_last_state;
	handle->stateConfVectorPosition = 0;
}

/* Default exit sequence for region main region */
static void blinky_exseq_main_region(Blinky* handle)
{
	/* Default exit sequence for region main region */
	/* Handle exit of all possible states (of blinky.main_region) at position 0... */
	switch(handle->stateConfVector[ 0 ])
	{
		case Blinky_main_region_ON :
		{
			blinky_exseq_main_region_ON(handle);
			break;
		}
		case Blinky_main_region_OFF :
		{
			blinky_exseq_main_region_OFF(handle);
			break;
		}
		default: break;
	}
}

/* The reactions of state ON. */
static void blinky_react_main_region_ON(Blinky* handle)
{
	/* The reactions of state ON. */
	if (blinky_check_main_region_ON_tr0_tr0(handle) == bool_true)
	{ 
		blinky_effect_main_region_ON_tr0(handle);
	}  else
	{
		if (blinky_check_main_region_ON_tr1_tr1(handle) == bool_true)
		{ 
			blinky_effect_main_region_ON_tr1(handle);
		} 
	}
}

/* The reactions of state OFF. */
static void blinky_react_main_region_OFF(Blinky* handle)
{
	/* The reactions of state OFF. */
	if (blinky_check_main_region_OFF_tr0_tr0(handle) == bool_true)
	{ 
		blinky_effect_main_region_OFF_tr0(handle);
	}  else
	{
		if (blinky_check_main_region_OFF_tr1_tr1(handle) == bool_true)
		{ 
			blinky_effect_main_region_OFF_tr1(handle);
		} 
	}
}

/* Default react sequence for initial entry  */
static void blinky_react_main_region__entry_Default(Blinky* handle)
{
	/* Default react sequence for initial entry  */
	handle->internal.iTick = 0;
	blinky_enseq_main_region_ON_default(handle);
}


