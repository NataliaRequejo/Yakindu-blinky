
#ifndef BLINKY_H_
#define BLINKY_H_

#include "sc_types.h"
		
#ifdef __cplusplus
extern "C" { 
#endif 

/*! \file Header of the state machine 'blinky'.
*/

/*! Enumeration of all states */ 
typedef enum
{
	Blinky_main_region_ON,
	Blinky_main_region_OFF,
	Blinky_last_state
} BlinkyStates;

/*! Type definition of the data structure for the BlinkyInternal interface scope. */
typedef struct
{
	sc_integer iTick;
} BlinkyInternal;

/*! Type definition of the data structure for the BlinkyIface interface scope. */
typedef struct
{
	sc_boolean evTick_raised;
} BlinkyIface;


/*! Define dimension of the state configuration vector for orthogonal states. */
#define BLINKY_MAX_ORTHOGONAL_STATES 1

/*! 
 * Type definition of the data structure for the Blinky state machine.
 * This data structure has to be allocated by the client code. 
 */
typedef struct
{
	BlinkyStates stateConfVector[BLINKY_MAX_ORTHOGONAL_STATES];
	sc_ushort stateConfVectorPosition; 
	
	BlinkyInternal internal;
	BlinkyIface iface;
} Blinky;

/*! Initializes the Blinky state machine data structures. Must be called before first usage.*/
extern void blinky_init(Blinky* handle);

/*! Activates the state machine */
extern void blinky_enter(Blinky* handle);

/*! Deactivates the state machine */
extern void blinky_exit(Blinky* handle);

/*! Performs a 'run to completion' step. */
extern void blinky_runCycle(Blinky* handle);


/*! Raises the in event 'evTick' that is defined in the default interface scope. */ 
extern void blinkyIface_raise_evTick(Blinky* handle);


/*!
 * Checks whether the state machine is active (until 2.4.1 this method was used for states).
 * A state machine is active if it was entered. It is inactive if it has not been entered at all or if it has been exited.
 */
extern sc_boolean blinky_isActive(const Blinky* handle);

/*!
 * Checks if all active states are final. 
 * If there are no active states then the state machine is considered being inactive. In this case this method returns false.
 */
extern sc_boolean blinky_isFinal(const Blinky* handle);

/*! Checks if the specified state is active (until 2.4.1 the used method for states was called isActive()). */
extern sc_boolean blinky_isStateActive(const Blinky* handle, BlinkyStates state);

#ifdef __cplusplus
}
#endif 

#endif /* BLINKY_H_ */
