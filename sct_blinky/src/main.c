/* Copyright 2017, Natalia Requejo
 * Basado en el ejemplo de 2016 de Pablo Ridolfi
 * All rights reserved.
 *
 * This file is part of Workspace.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "main.h"
#include "chip.h"


/** statechart instance */
static Blinky statechart;

/** clock and peripherals initialization */
static void initHardware(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / 1000);

	Chip_SCU_PinMux(  2 ,  11 , MD_PUP | MD_EZI , FUNC0 );
	Chip_GPIO_Init(LPC_GPIO_PORT);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 11);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false);
}

void SystemInit(void)
{
#if defined(CORE_M3) || defined(CORE_M4)
	unsigned int *pSCB_VTOR = (unsigned int *) 0xE000ED08;

#if defined(__IAR_SYSTEMS_ICC__)
	extern void *__vector_table;

	*pSCB_VTOR = (unsigned int) &__vector_table;
#elif defined(__CODE_RED)
	extern void *g_pfnVectors;

	*pSCB_VTOR = (unsigned int) &g_pfnVectors;
#elif defined(__ARMCC_VERSION)
	extern void *__Vectors;

	*pSCB_VTOR = (unsigned int) &__Vectors;
#endif

#if defined(__FPU_PRESENT) && __FPU_PRESENT == 1
	fpuInit();
#endif

#if defined(NO_BOARD_LIB)
	/* Chip specific SystemInit */
	Chip_SystemInit();
#else
	/* Board specific SystemInit */
	//Board_SystemInit();
#endif

#endif /* defined(CORE_M3) || defined(CORE_M4) */
}

/*==================[external functions definition]==========================*/
/** state machine user-defined external function (action) */
void blinkyIface_opLED(Blinky* handle, const sc_boolean onoff)
{
	//LED - ON -OFF
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) onoff);
}

/** SysTick interrupt handler */
void SysTick_Handler(void)
{
	/* Evento Tick necesario en la maquina de estado*/
	blinkyIface_raise_evTick(&statechart);
	/* update state machine */
	blinky_runCycle(&statechart);
}

int main(void){
	/* init and reset state machine */
	blinky_init(&statechart);
	blinky_enter(&statechart);

	initHardware();

	while (1) {
		__WFI(); /* wait for interrupt */
	}
}
